package polymorphism;

public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("The Alchemist", "Cohelo");
        books[1] = new ElectronicBook("100 years of solitude", "Gabriel", 43);
        books[2] = new Book("Crime and Punishment", "A Russian dude");
        books[3] = new ElectronicBook("Harry Potter", "J.K.", 120);
        books[4] = new ElectronicBook("Blind Owl", "Sadegh Hedayat", 10);
        for (Book s : books) {
            System.out.println(s);
        }
        ElectronicBook e = (ElectronicBook) books[0];
        e.getNumnerBytes();
    }

}
