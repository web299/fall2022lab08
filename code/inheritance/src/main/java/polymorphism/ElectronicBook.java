package polymorphism;

public class ElectronicBook extends Book {
    private double numberBytes;

    public ElectronicBook(String title, String author, double numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public double getNumnerBytes() {
        return this.numberBytes;
    }

    @Override
    public String toString() {
        String formBase = super.toString();
        return formBase + " number of bytes: " + this.numberBytes;
    }
}
